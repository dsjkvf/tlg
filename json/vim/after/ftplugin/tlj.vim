" Options
set showtabline=0
function! StatusLine()
    let msg = 'gi: Inspect    gf: Filter    j/k: Next/Previous    gd: Dump    q: Quit'
    let line = ''
    let line .= repeat(' ', (&columns - len(msg))/2->float2nr())
    let line .= msg
    return line
endfunction
set statusline=%#Question#%{StatusLine()}
set nolist
set cursorline
set matchpairs=

" Mappings
" view details
nnoremap <buffer> <silent> gi :noautocmd let z = getline('.')->matchstr('http.*') \|
             \ noautocmd exe 'tab term tlj info ' . z \|
             \ setlocal nocursorline<CR>
nmap <buffer> <CR> gi
" view filtered
nnoremap <buffer> <silent> gf :let c = matchstr(getline('.'), '.*\d\d\d\d-\d\d-\d\d \d\d:\d\d \zs[^ ]\+\ze .*') \|
             \ let z = map(getline(1, '$'), "v:val =~ c ? v:val : ''")->filter('len(v:val) > 0') \|
             \ enew \|
             \ silent! put =z \|
             \ 1delete \|
             \ set ft=tlj<CR>
" move
nmap <silent> <expr> k getline(1) =~ '^https://' && &buftype == 'terminal'
            \ ? 'q<Up>gi'
            \ : 'k'
nmap <silent> <expr> j getline(1) =~ '^https://' && &buftype == 'terminal'
            \ ? 'q<Down>gi'
            \ : 'j'
" dump
nnoremap <expr> gd getline(1) =~ '^https://' && &buftype == 'terminal'
            \ ? ':exe "w ". expand("$XDG_CACHE_HOME/tlj/LOGs/") . getline(1)->split("/")[-2:]->join("-") . "-" . strftime("%s", strftime(localtime())) . ".txt"<CR>'
            \ : ':exe "w ". expand("$XDG_CACHE_HOME/tlj/LOGs/") . "FEEDS-" . strftime("%s", strftime(localtime())) . ".txt"<CR>'
" open
nmap <silent> <expr> gu getline(1) =~ '^https://' && &buftype == 'terminal'
            \ ? 'gggx'
            \ : 'gx'
" quit
nnoremap <silent> <expr> q getbufinfo({'buflisted':1})->len() > 1 ? ':bd<CR>:redraw!<CR>' : ':q!<CR>'
