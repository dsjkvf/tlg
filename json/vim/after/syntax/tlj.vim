" Syntax
syntax match  tljURL            /\thttp.*$/ conceal
syntax match  tljNew            /^• / conceal
syntax match  tljDate           /^\d\d\d\d-\d\d-\d\d \d\d:\d\d/
syntax match  tljDateNew        /^• \d\d\d\d-\d\d-\d\d \d\d:\d\d/ contains=tljNew
syntax region tljAuthor         start=/^\d\d\d\d-\d\d-\d\d \d\d:\d\d / end=/ / contains=tljDate keepend
syntax region tljAuthorNew      start=/^• \d\d\d\d-\d\d-\d\d \d\d:\d\d / end=/ / contains=tljNew,tljDateNew keepend
syntax region tljTitle          start=/^\d\d\d\d-\d\d-\d\d \d\d:\d\d [A-Za-z0-9_]\+ / end=/\thttp.*$/ contains=tljDate,tljAuthor,tljURL keepend
syntax region tljTitleNew       start=/^• \d\d\d\d-\d\d-\d\d \d\d:\d\d [A-Za-z0-9_]\+ / end=/\thttp.*$/ contains=tljNew,tljDateNew,tljAuthorNew,tljURL keepend

" Highlights
hi! def link tljDate            Comment
hi! def link tljDateNew         Comment
hi! def link tljAuthor          Title
hi! def link tljAuthorNew       Title
hi! def link tljTitle           String
hi! def link tljTitleNew        Question

" Options
set conceallevel=3
set concealcursor=nv
