#!/bin/bash

# set the conf dir
[ -z $XDG_CONFIG_HOME ] && XDG_CONFIG_HOME=$HOME/.config
! [ -d $XDG_CONFIG_HOME/tlj ] && mkdir -p $XDG_CONFIG_HOME/tlj
CONF_DIR=$XDG_CONFIG_HOME/tlj

# set the cache dir
[ -z $XDG_CACHE_HOME ] && XDG_CACHE_HOME=$HOME/.config
! [ -d $XDG_CACHE_HOME/tlj ] && mkdir -p $XDG_CACHE_HOME/tlj
CACHE_DIR=$XDG_CACHE_HOME/tlj

# set the command
[ -z $1 ] && COM='list' || COM=$1

# set Vim
[ -z $VIMBIN ] && VIMBIN='vim'

# set the date program
date --version 2> /dev/null | grep -q GNU && PAR='-d@' || PAR='-r'

case $COM in
    list)
        # get the current last entries
        OLD=$(cat $CONF_DIR/last)
        # dwnload feeds and parse entries
        for i in $(cat $CONF_DIR/conf | grep '^http')
        do
            # get the feed's name
            FEED=$(basename $i)
            # get the feed's base URL
            URL=$(echo $i | sed 's,https://tg.i-c-a.su/json/\(.*\),https://t.me/\1,')
            # collect feeds
            curl -s $i > $CACHE_DIR/$FEED
            # parse over the cached feeds
            IFS=$'\n' temp=($(cat $CACHE_DIR/$FEED | \
                jq -c '.messages[] | [.id, .date, .message] | join("†")' |\
                awk -v FS="†" \
                    -v PAR=$PAR \
                    -v URL="$URL" \
                    -v FEED=$FEED '{d = "date "PAR$2" +\"%Y-%m-%d %H:%M\""; \
                                        d | getline rez; close(d); \
                                        match($3, /(.[^!?.:;<]*)(.).*/, arr); \
                                        if (length(arr) > 0) { \
                                            print rez" "FEED" "arr[1]arr[2]"	"URL"/"substr($1, 2) \
                                        }}'))
            # explicitly collect the last entry of every subscription
            last="${last} ${temp[0]}"
            # and collect all the entries from every subscription
            news=("${news[@]}" "${temp[@]}")
        done

        # open results in Vim
        for j in ${news[@]}; do echo "$j"; done | \
            sort -r | \
            awk -v OLD="$OLD" '{if ($0 ~ OLD) {old = 1}; \
                                gsub(/<[^>]*>/, "", $0); \
                                gsub(/<.*http/, "	http", $0); \
                                gsub(/\\n/, "", $0); \
                                gsub(/\\/, "", $0); \
                                if (old) {print $0} else {print "• "$0}}' | \
            $VIMBIN +'set ft=tlj' +'echo ""' - --not-a-term
        # set last (any entry newer than those will be marked with '•' next time)
        echo "$last" | \
            grep -o 'http[^ ]*' | \
            awk -v FS='/' '{if (NR>1) {printf "|"$4"/"$5} else {printf $4"/"$5}}' > $CONF_DIR/last
        ;;
    info)
        # check if the URL was provided, exit otherwise
        ! [ -z $2 ] && URL=$(echo $2 | grep -o 'http.*$') || exit 1
        printf "$URL\n\n"
        # get the entry's ID
        ID=$(basename $URL)
        # get the feed's name
        FEED=$(echo $URL | sed 's,https://t.me/\(.[^/]*\)/.*,\1,')
        # parse the cached feed
        IFS=$'\n' temp=($(cat $CACHE_DIR/$FEED | jq -rc ".messages[] | select(.id == $ID) | .date, .message" 2> /dev/null))
        # print out the results
        if ! [ -z ${temp[0]} ]
        then
            printf "$(date -r ${temp[0]} +"%Y-%m-%d %H:%M")\n\n"
            # strip
            temp=${temp[@]:1}
            # clean
            temp="${temp//​/}"
            temp="${temp//‌/}"
            temp="${temp//‍/}"
            temp="${temp//‎/}"
            # print
            echo $temp | w3m -T text/html \
                            -cols 100 \
                            -dump \
                            -o display_image=false \
                            -o display_link=true \
                            -o display_link_number=true \
                            -o decode_url=yes
        else
            printf "ERROR: reading message contents failed"
        fi
        ;;
esac
