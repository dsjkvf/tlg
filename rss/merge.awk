#!/bin/awk -f

BEGIN {
    while (getline < ARGV[2]) {
        if ($0 ~ "<item>") {
            start = 1
        }
        if (start) {
            items = items$0
        }
        if ($0 ~ "</item>") {
            items = items"|"
            start = 0
        }
        if (items == "") {
            header = header$0
        }
    }

    while (getline < ARGV[1]) {
        if ($0 ~ "<item>") {
            start = 1
        }
        if (start) {
            items = items$0
        }
        if ($0 ~ "</item>") {
            items = items"|"
            start = 0
        }
    }

    split(items, array, "|")

    # here we add `array[i]` as a key to the `seen` dict and after that immediately increment it to 1
    # because empty keys in AWK's dicts are non-distinguishable from non-existent keys
    #
    # by doing this, it will be 0 with the very first check (resulting as TRUE in the `!` reversed conditional)
    # because the right-side increment happens after an operation
    # and after that, it will become 1, 2 and so on with the every subsequent check (resulting as FALSE)
    for (i=1; i in array; i++) {
        if ( !seen[array[i]]++ ) {
            unique[++j] = array[i]
        }
    }

    print header

    for (i=1; i in unique; i++) {
        # if we encounter "normal" RSS entry
        if (unique[i] ~ "<item>" && unique[i] ~ "</item>") {
            # count it
            ++counter
            # and then stop merging if we already have 40 such entries
            if (counter == 40) {
                break
            }
        }
        if (length(unique[i]) > 0) {
            print unique[i]
        }
    }

    print "</channel></rss>"
}
